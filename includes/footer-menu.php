<div class="footer-menu footer-menu-dark">
    <a href="index.html"><i class="fa fa-home"></i><em>Home</em></a>
    <a href="features-typography.html"><i class="fa fa-cog"></i><em>Features</em></a>
    <a href="page-soon.html"><i class="fa fa-files-o"></i><em>Pages</em></a>
    <a href="gallery-blocks.html" class="phablet-item"><i class="fa fa-camera"></i><em>Gallery</em></a>
    <a href="portfolio-adaptive.html" class="tablet-item"><i class="fa fa-image"></i><em>Portfolio</em></a>
    <a href="pageapp-coverpage.html" class="tablet-item"><i class="fa fa-camera"></i><em>AppStyle</em></a>
    <a href="contact.html"><i class="fa fa-envelope-o"></i><em>Contact</em></a>
    <a href="#" class="show-menu"><i class="fa fa-navicon"></i><em>Menu</em></a>
</div>