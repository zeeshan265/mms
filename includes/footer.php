 <div class="footer footer-light">
            <div class="footer-content">
                <a href="#" class="footer-logo"></a>
                <p class="footer-text">
                   MMS-App allows users to find and book pre-qualified, trusted, and reviewed wedding vendors, get special prices, and find wedding inspirations to realize your dream wedding.
                </p>
            </div>
            <div class="footer-socials">
                <a href="#" class="default-link scale-hover facebook-color"><i class="fa fa-facebook"></i></a>
                <a href="#" class="default-link scale-hover twitter-color"><i class="fa fa-twitter"></i></a>
                <a href="#" class="default-link scale-hover google-color"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="default-link scale-hover phone-color"><i class="fa fa-instagram"></i></a>
                <a href="#" class="scale-hover mail-color "><i class="fa fa-linkedin"></i></a>
                <a href="#" class="scale-hover bg-night-dark back-to-top"><i class="fa fa-angle-up"></i></a>
                <div class="clear"></div>
            </div>
            <p class="footer-strip">Copyright <span id="copyright-year"></span> Consult things. All Rights Reserved</p>
        </div>
    </div>
</div>


    
</div>
<a href="#" class="back-to-top-badge"><i class="fa fa-caret-up"></i>Back to top</a></body>