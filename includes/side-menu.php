<div class="launchpad launchpad-dark">
    <div class="launchpad-scroll">
        
        <a href="index.html" class="menu-logo"></a>
        <em class="menu-subtitle">Creative Simplicity</em>
        <a href="#" class="close-menu menu-close-top"><i class="fa fa-times"></i></a>

        <a class="menu-item" href="index.html">
            <i class="fa fa-star-o"></i>
            <em>Welcome</em>
            <i class="fa fa-circle"></i>
        </a>
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu active-menu-item">
                <i class="fa fa-home"></i>
                <em>Homepage</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu active-submenu">     
                <a href="index-cover.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Cover</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="index-classic.html" class="menu-item active-menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Classic</em>
                    <i class="fa fa-circle"></i>
                </a>
                <a href="index-circle.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Rounded</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="index-image.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Image</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="index-video.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Video</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="index-news.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="index-shop.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store</em>
                    <i class="fa fa-circle"></i>
                </a>                                   
                <a href="index-login.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Login</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="index-signup.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Signup</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="index-splash.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Splash</em>
                    <i class="fa fa-circle"></i>
                </a>                 
                <a href="index-landing.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Landing</em>
                    <i class="fa fa-circle"></i>
                </a>                
            </div>
        </div>        
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-cog"></i>
                <em>Features</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="features-typography.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Type</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="features-tabs.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Tabs</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="features-toggles.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Toggles</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="features-accordion.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Accordion</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="features-decorations.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Decorations</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="features-notification.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Notifications</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="features-charts.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Charts & Stats</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="features-buttons.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Buttons & Icons</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="features-inputs.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Input Elements</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="features-others.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Other Features</em>
                    <i class="fa fa-circle"></i>
                </a>                                
            </div>
        </div>
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-eye"></i>
                <em>Page Options</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="skin-dark.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Skin Dark</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="skin-light.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Skin Light</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="skin-material.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Skin Material</em>
                    <i class="fa fa-circle"></i>
                </a>                             
                <a href="#" class="menu-item change-menu-light">
                    <i class="fa fa-angle-right"></i>
                    <em>Menu Light</em>
                    <i class="fa fa-circle"></i>
                </a>                    
                <a href="#" class="menu-item change-menu-dark">
                    <i class="fa fa-angle-right"></i>
                    <em>Menu Dark</em>
                    <i class="fa fa-circle"></i>
                </a>                 
                <a href="#" class="menu-item change-menu-boxed">
                    <i class="fa fa-angle-right"></i>
                    <em>Boxed Menu</em>
                    <i class="fa fa-circle"></i>
                </a>                    
                <a href="#" class="menu-item change-menu-full">
                    <i class="fa fa-angle-right"></i>
                    <em>Full Screen Menu</em>
                    <i class="fa fa-circle"></i>
                </a>                                
            </div>
        </div>     
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-camera"></i>
                <em>Gallery</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="gallery-round.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Round</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="gallery-square.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Square</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="gallery-blocks.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Blocks</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="gallery-adaptive.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Adaptive</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="gallery-filter.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Filterable</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="gallery-wide.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Widescreen</em>
                    <i class="fa fa-circle"></i>
                </a>                                                   
            </div>
        </div>        
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-image"></i>
                <em>Portfolio</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="portfolio-one.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>One Item</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="portfolio-two.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Two Items</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="portfolio-adaptive.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Adaptive</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="portfolio-filter.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Filterable</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="portfolio-wide.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Widescreen</em>
                    <i class="fa fa-circle"></i>
                </a>                                                                  
            </div>
        </div>        
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-files-o"></i>
                <em>Site Pages</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="page-error.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>404 Page</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="page-soon.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Coming Soon</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="page-signup.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Register Page</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="page-login.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Login Page</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="page-reviews.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>User Reviews</em>
                    <i class="fa fa-circle"></i>
                </a>                                                                                   
            </div>
        </div>        
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-mobile"></i>
                <em>AppStyled</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="pageapp-signup.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Register Screen</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="pageapp-login.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Login Screen</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="pageapp-coverpage.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Coverpage</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="pageapp-calendar.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Calendar</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="pageapp-timeline-1.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Timeline Left</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="pageapp-timeline-2.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Timeline Center</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="pageapp-profile-1.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>User Profile 1</em>
                    <i class="fa fa-circle"></i>
                </a>                 
                <a href="pageapp-profile-2.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>User Profile 2</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="pageapp-userlist.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>User Lists</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="pageapp-activity.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Activity Page</em>
                    <i class="fa fa-circle"></i>
                </a>                                                                
            </div>
        </div>        
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-shopping-cart"></i>
                <em>Store Pages</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="store-front1.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Front 1</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="store-front2.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Front 2</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="store-cover.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Coverpage</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="store-card.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Cards</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="store-product.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Product</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="store-cart1.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Cart 1</em>
                    <i class="fa fa-circle"></i>
                </a>                 
                <a href="store-cart2.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Cart 2</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="store-history.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store History</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="store-checkout.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store Checkout</em>
                    <i class="fa fa-circle"></i>
                </a>                 
                <a href="store-faq.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store FAQ</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="store-about.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>Store About</em>
                    <i class="fa fa-circle"></i>
                </a>                                                                
            </div>
        </div>        
        <div class="has-submenu">
            <a href="#" class="menu-item open-submenu">
                <i class="fa fa-newspaper-o"></i>
                <em>News Pages</em>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="submenu">
                <a href="news-index.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Front</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="news-cover.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Cover</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="news-list-1.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Article List 1</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="news-list-2.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Article List 2</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="news-list-3.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Article List 3</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="news-post-1.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Article Post 1</em>
                    <i class="fa fa-circle"></i>
                </a>                 
                <a href="news-post-2.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Article Post 2</em>
                    <i class="fa fa-circle"></i>
                </a>                
                <a href="news-post-3.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Article Post 3</em>
                    <i class="fa fa-circle"></i>
                </a>                   
                <a href="news-archive.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News Archive</em>
                    <i class="fa fa-circle"></i>
                </a>                 
                <a href="news-faq.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News FAQ</em>
                    <i class="fa fa-circle"></i>
                </a>                  
                <a href="news-about.html" class="menu-item">
                    <i class="fa fa-angle-right"></i>
                    <em>News About</em>
                    <i class="fa fa-circle"></i>
                </a>                                                                
            </div>
        </div>
        <a href="page-blog.html" class="menu-item">
            <i class="fa fa-pencil"></i>
            <em>Blog</em>
            <i class="fa fa-circle"></i>
        </a>        
        <a href="page-videos.html" class="menu-item">
            <i class="fa fa-youtube-play"></i>
            <em>Videos</em>
            <i class="fa fa-circle"></i>
        </a>         
        <a href="page-map.html" class="menu-item">
            <i class="fa fa-map-marker"></i>
            <em>Map</em>
            <i class="fa fa-circle"></i>
        </a>          
        <a href="contact.html" class="menu-item">
            <i class="fa fa-envelope-o"></i>
            <em>Contact</em>
            <i class="fa fa-circle"></i>
        </a>         
        <a href="#" class="menu-item close-menu">
            <i class="fa fa-times"></i>
            <em>Close</em>
            <i class="fa fa-circle"></i>
        </a>  
    </div>
</div> 