<?php
include("includes/header.php");
include("includes/footer-menu.php");
include("includes/side-menu.php");
?>
        <div class="cover-page cover-image" style="background-image:url(images/pictures/abc.jpg);">
            <div class="cover-page-content">

                <div class="pageapp-login">
                    <a href="#" class="pageapp-login-logo"></a>
                    <div class="spacer"></div>
                    <div class="cover-field">
                        <i class="fa fa-user"></i>
                        <input type="text" value="Username">
                    </div>                
                    <div class="cover-field full-bottom">
                        <i class="fa fa-lock"></i>
                        <input type="password" value="passswordhere">
                    </div>
                    <a href="#" class="pageapp-login-button button button-green"><i class="fa fa-arrow-right"></i>Login</a>
                    <div class="decoration half-bottom"></div>
                    <a href="#" class="pageapp-login-button button facebook-color"><i class="fa fa-facebook"></i>Login with Facebook</a>
                    <a href="#" class="pageapp-login-button button twitter-color"><i class="fa fa-twitter"></i>Login with Twitter</a>
                </div>

            </div>
            <div class="overlay"></div>
        </div>
    
