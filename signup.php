<?php
include("includes/header.php");
include("includes/footer-menu.php");
include("includes/side-menu.php");
?>
<div id="page-content" class="page-content">
    <div id="page-content-scroll">
        <div class="cover-page cover-image" style="background-image:url(images/pictures/abc.jpg);">
            <div class="cover-page-content"><!--add boxed-layout to this to change the style-->

                <div class="pageapp-login">
                    <a href="#" class="pageapp-login-logo"></a>
                    <div class="spacer"></div>
                    <div class="cover-field">
                        <i class="fa fa-user"></i>
                        <input type="text" value="Username">
                    </div>                    
                    <div class="cover-field">
                        <i class="fa fa-envelope-o"></i>
                        <input type="text" value="Email">
                    </div>                
                    <div class="cover-field">
                        <i class="fa fa-lock"></i>
                        <input type="password" value="passswordhere">
                    </div>                
                    <div class="cover-field full-bottom">
                        <i class="fa fa-calendar-o"></i>
                        <input class="set-today" type="date">
                    </div>
                    <a href="#" class="pageapp-login-button button button-green"><i class="fa fa-arrow-right"></i>Register</a>
                </div>

            </div>
            <div class="overlay"></div>
        </div>
    </div>
</div>