<?php
include("includes/header.php");
include("includes/footer-menu.php");
include("includes/side-menu.php");
?>

        
        <div class="homepage-slider full-bottom">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img class="responsive-image" src="images/pictures/margalla.jpg" alt="img">
                    <div class="slider-caption">
                        <h3 class="center-text text-uppercase">Margalla Grande Marquee</h3>
                        <p class="center-text"> Near PSO Petrol Pump, Islamabad Highway, Islamabad.</p>
                    </div>
                    <div class="overlay"></div>
                </div>
                <div class="swiper-slide">
                    <img class="responsive-image" src="images/pictures/sarena.jpg" alt="img">
                    <div class="slider-caption">
                        <h3 class="left-text text-uppercase">Serena Hotel Islamabad </h3>
                        <p class="left-text"> Khayaban-e-Suhrwardy, Opposite Convention Centre Islamabad 44000</p>
                    </div>
                    <div class="overlay"></div>
                </div>
                <div class="swiper-slide">
                    <img class="responsive-image" src="images/pictures/venue.jpg" alt="img">
                    <div class="slider-caption">
                        <h3 class="right-text text-uppercase">Venue One</h3>
                        <p class="right-text"> Venue One, Street 787, Islamabad.</p>
                    </div>
                    <div class="overlay"></div>
                </div>
            </div>
        </div>
        
<?php 

include("includes/footer.php");
 ?>